

const { ccclass, property } = cc._decorator;

@ccclass
export default class StreamerCard extends cc.Component {

    private _curB: number = 1.0;
    private _minB: number = -1.0;
    private _maxB: number = 1.0;
    private _material: cc.Material = null;
    private _playing: boolean = false;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

        this._playing = true;
        this._material = this.node.getComponent(cc.Sprite).getMaterial(0);

        this._material.setProperty("k", 1);
    }

    // start() {
    //     this._material = this.node.getComponent(cc.Sprite).getMaterial(0);

    //     this._material.setProperty("k", 1);
    // }

    update(dt) {

        if (!this._playing) {
            return;
        }

        if (this.node.active && this._material != null && this._playing) {
            //设置参数b
            // cc.log("设置b:", this._curB);
            this._material.setProperty("b", this._curB);
        }
        this._curB -= 0.03;
        if (this._curB <= this._minB) {
            this._curB = this._maxB;
            this._material.setProperty("b", this._curB);

            this._playing = false;
            console.log("curB:", this._curB);
            this.scheduleOnce(() => {
                this._playing = true;
            }, 3);
        }

    }
}
