// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import LifeCallBack from "./LifeCallBack";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TestSceneForLifeCallBack extends cc.Component {

    @property(cc.Node)
    ndSprite: cc.Node = null;


    onClickBtnShowOrHide() {
        // return;
        this.ndSprite.active = !this.ndSprite.active;

        this.scheduleOnce(() => {
            cc.log("组件 enabled:", this.ndSprite.getComponent(LifeCallBack).enabled);
        }, 1);


    }

    onClickBtnDestroy() {
        this.ndSprite.destroy();
    }

    onClickBtnEnable() {
        let comp = this.ndSprite.getComponent(LifeCallBack);
        comp && (comp.enabled = !comp.enabled);
    }

}
