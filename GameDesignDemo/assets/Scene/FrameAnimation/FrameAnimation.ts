// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class FrameAnimation extends cc.Component {

    _spriteFrames: cc.SpriteFrame[] = [];
    _frameDuration: number = 1;
    _playing: boolean = false;
    _totalFrameNum: number = 0;
    _curFrameNum: number = 0;
    _isLoop: boolean = true;

    setData(spriteFrames: cc.SpriteFrame[], totalTimeSeconds: number = 1, isLoop: boolean = true) {
        this._spriteFrames = spriteFrames;

        if (totalTimeSeconds) {

            let frameRate = cc.game.getFrameRate();

            let eachSprFrameSecond = totalTimeSeconds / this._spriteFrames.length;
            this._frameDuration = Math.floor(eachSprFrameSecond * frameRate);
        }

        this._totalFrameNum = this._frameDuration * this._spriteFrames.length;
        this._curFrameNum = 0;

        this._isLoop = isLoop;
    }

    play() {
        this._curFrameNum = 0;
        this._playing = true;
    }

    update(dt) {
        if (this._playing) {
            if (this._curFrameNum % this._frameDuration == 0 && this._curFrameNum < this._totalFrameNum) {
                cc.log("frameIndex:", this._curFrameNum / this._frameDuration);
                this.node.getComponent(cc.Sprite).spriteFrame = this._spriteFrames[this._curFrameNum / this._frameDuration];
            }

            this._curFrameNum++;

            if (this._curFrameNum >= this._totalFrameNum) {

                if (this._isLoop) {
                    this._curFrameNum = 0;
                } else {
                    this._playing = false;
                }
            }
        }
    }
}
