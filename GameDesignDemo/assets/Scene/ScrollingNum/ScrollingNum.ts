

const { ccclass, property } = cc._decorator;

@ccclass
export default class LabelScrolllingNum extends cc.Component {

    private _num: number = 0;

    private _toNum: number = 0;

    private _delta: number = 0;

    private lbNum: cc.Label = null;

    private MY_DT: number = 0.1;


    public scrollToNum(curNum: number, nextNum: number, delta: number, interval: number) {
        this.unschedule(this.updateNum);

        this._num = curNum;

        this.lbNum = this.node.getComponent(cc.Label);
        if (!this.lbNum) {
            this.lbNum = this.node.addComponent(cc.Label);
        }

        this.lbNum.string = this._num + "";


        this._delta = Math.ceil(delta);
        if (this._delta == 0) {
            this._delta = 1;
        }

        this._toNum = nextNum;

        this.MY_DT = interval;

        this.schedule(this.updateNum, this.MY_DT);
    }

    private updateNum() {

        this._num += this._delta;

        if (this._num > this._toNum) {
            this._num = this._toNum;
        }
        this.lbNum.string = this._num + "";
        if (this._num >= this._toNum) {
            this.unschedule(this.updateNum);
        }
    }

    onDestroy() {
        this.unschedule(this.updateNum);
    }


    // update (dt) {}
}
