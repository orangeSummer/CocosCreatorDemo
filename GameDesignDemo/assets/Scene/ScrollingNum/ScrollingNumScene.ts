// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import LabelScrolllingNum from "./ScrollingNum";


const { ccclass, property } = cc._decorator;

@ccclass
export default class ScrolllingNumScene extends cc.Component {

    @property(cc.Node)
    ndScrollNum: cc.Node = null;

    _num: number = 0;

    start() {
        this.ndScrollNum.getComponent(cc.Label).string = this._num + "";
    }

    onClickBtnStart() {
        let curNum = this._num;
        this._num += 300;

        let nextNum = this._num;
        this.scrollToNum2(curNum, nextNum);
    }


    scrollToNum2(curNum: number, nextNum: number) {
        this.ndScrollNum.getComponent(LabelScrolllingNum).scrollToNum(curNum, nextNum, 1);
    }

    // update (dt) {}
}
