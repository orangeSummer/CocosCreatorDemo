// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class FlipCardScene extends cc.Component {

    @property(cc.Node)
    ndCard: cc.Node = null;

    start() {

    }

    click() {
        let scaleX = this.ndCard.scaleX;

        cc.tween(this.ndCard).to(1, {
            scaleX: -scaleX,
        }).start();
    }



    // update (dt) {}
}
